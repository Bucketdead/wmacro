﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;

namespace wMACRO
{
    /// <summary>
    /// Logique d'interaction pour LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : Window
    {
        // SYSTEM :
        string formTitre = File.ReadAllText("D:/C#/wMACRO/wMACRO/system/titre.txt", Encoding.UTF8);
        string formSlogan = File.ReadAllText("D:/C#/wMACRO/wMACRO/system/slogan.txt", Encoding.UTF8);
        string formVersion = File.ReadAllText("D:/C#/wMACRO/wMACRO/system/version.txt", Encoding.UTF8);

        int timerValue = 0;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();

        public LoadingWindow()
        {
            InitializeComponent();
        }

        private void LoadingForm_Loaded(object sender, RoutedEventArgs e)
        {
            LoadingForm.Title = formTitre + " - Chargement... [" + formVersion + "]";
            TxtTitre.Text = formTitre;
            TxtSlogan.Text = formSlogan;
            TxtTexte.Text = "Version : " + formVersion;

            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = TimeSpan.FromMilliseconds(5);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (LWProgress.Value < 1000)
            {
                timerValue = timerValue + 1;
                LWProgress.Value = timerValue;
            }
            else
            {
                dispatcherTimer.Stop();
                this.Close();
            }

            if (LWProgress.Value == 200)
            {
                TxtTexte.Text = "Chargement des modules...";
            }
            if (LWProgress.Value == 300)
            {
                TxtTexte.Text = "Connexion à la base de donnés...";
            }
            if (LWProgress.Value == 600)
            {
                TxtTexte.Text = "Recherche de mises à jour...";
            }
            if (LWProgress.Value == 800)
            {
                MainWindow wMain = new MainWindow();
                wMain.Show();
            }
        }
    }
}
