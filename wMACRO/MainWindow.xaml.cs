﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;

namespace wMACRO
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //
        //
        #region SYSTEME :

        string formTitre = File.ReadAllText("D:/C#/wMACRO/wMACRO/system/titre.txt", Encoding.UTF8);
        string formSlogan = File.ReadAllText("D:/C#/wMACRO/wMACRO/system/slogan.txt", Encoding.UTF8);
        string formVersion = File.ReadAllText("D:/C#/wMACRO/wMACRO/system/version.txt", Encoding.UTF8);

        // SaveFileDialog :
        private void SFD()
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Sauvegarder le macro ?", formTitre + " - Sauvegarder - [" + formVersion + "]", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel);
            switch (messageBoxResult)
            {
                case MessageBoxResult.Yes:
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.FileName = "wMACRO_New";
                    saveFileDialog.Filter = "Document texte (.txt)|*.txt";
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        File.WriteAllText(saveFileDialog.FileName, TxtFinal.Text);
                        TxtFinal.Text = "";
                    }
                    break;

                case MessageBoxResult.No:
                    TxtFinal.Text = "";
                    break;

                case MessageBoxResult.Cancel:
                    break;
            }
        }

        // OpenFileDialog :
        private void OFD()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Document texte (.txt)|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                TxtFinal.Text = File.ReadAllText(openFileDialog.FileName);
            }
        }
        private void OFDSave()
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Sauvegarder le macro ?", formTitre + " - Sauvegarder - [" + formVersion + "]", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel);
            switch (messageBoxResult)
            {
                case MessageBoxResult.Yes:
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.FileName = "wMACRO_New";
                    saveFileDialog.Filter = "Document texte (.txt)|*.txt";
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        File.WriteAllText(saveFileDialog.FileName, TxtFinal.Text);
                        TxtFinal.Text = "";
                        OFD();
                    }
                    break;

                case MessageBoxResult.No:
                    TxtFinal.Text = "";
                    OFD();
                    break;

                case MessageBoxResult.Cancel:
                    break;
            }
        }
        #endregion

        //
        //
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainForm_Loaded(object sender, RoutedEventArgs e)
        {
            MainForm.Title = formTitre + " - " + formSlogan + " - [" + formVersion + "]";
            LabTitre.Content = formTitre;
        }

        // NOUVEAU
        private void BtnNouveau_Click(object sender, RoutedEventArgs e)
        {
            if (TxtFinal.Text.Length > 0)
            {
                SFD();
            }
        }

        // OUVRIR
        private void BtnOuvrir_Click(object sender, RoutedEventArgs e)
        {
            if (TxtFinal.Text.Length > 0)
            {
                OFDSave();
            }
            else
            {
                OFD();
            }
        }

        // IMPORTER

        // SAUVEGARDER
    }
}
