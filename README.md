Bienvenue sur le fichier **README.md** du projet **WowMacro v2** :
# [ WoW MACRO Labo. ] "*Interdit aux Gobelins !*"

- Site : **http://bucketdead.free.fr/wow_auto/**
- Git : **https://gitlab.com/Bucketdead/wowmacro-v2**

![YAAAAAAAH!](https://gitlab.com/Bucketdead/wow-macros/raw/master/_LAPOSTE/preview_01.png)

## Description :
**La création de macros n'a jamais été aussi rapide, précise et facile !**

World of Warcraft est de plus en plus compétitif. Pour progresser et atteindre ses ambitions il est nécessaire d'optimiser son espace de jeu, en Pve comme en Pvp. Des outils sont à notre disposition, d'un côté les addons pour avoir une interface détaillée et lisible, et de l'autre les macros pour exécuter des actions plus rapides et précises. Mais pour créer des macros jusqu'à maintenant le seul moyen était de lire de sombres tutoriels sur des forums... ce n'est pas simple pour tout le monde.

C'est pourquoi j'ai décidé de créer ce programme. Ca me permet d'approfondir mes connaissances pour mes cours et si ça peut aider des joueurs dans l'optimisation de leur gameplay, je serrai ravis.

N'hésitez pas à critiquer. Bon jeu !

<br/>

## Téléchargements :

|**OS**|**Version**|**Liens**|
|:-:|:-:|:-|
|Windows|0.1a|[https://gitlab.com/Bucketdead/wow-macros/Windows/0.1a/](https://gitlab.com/Bucketdead/wow-macros/raw/master/_LAPOSTE/Wow-MACROS_setup.rar)|
|Mac|0.1a|[https://gitlab.com/Bucketdead/wow-macros/Mac/0.1a/](https://gitlab.com/Bucketdead/wow-macros/raw/master/_LAPOSTE/Wow-MACROS_setup.rar)|

> *Dossier* ***Release*** *: [https://gitlab.com/Bucketdead/wow-macros/Release/](https://gitlab.com/Bucketdead/wow-macros.git)*
